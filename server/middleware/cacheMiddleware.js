const winston = require("winston");
const http = require("../util/httpUtil");

module.exports = function(cache, runningRequests) {
  const updateCache = async function(query) {
    try {
      winston.info(`Updating cache with query ${query}`);
      const searchResults = await http.fetchMovies({ searchQuery: query });
      const cacheObj = {
        searchResults,
        timestamp: Date.now()
      };
      cache.set(query, cacheObj);
      runningRequests.delete(query);
    } catch (error) {
      winston.error(error);
    }
  };

  const fetchKey = key => {
    return runningRequests.has(key);
  };
  const putKey = key => {
    return runningRequests.set(key, key);
  };

  const delay = async time => {
    return new Promise(resolve => setTimeout(resolve, time));
  };
  const delayConcurrentTask = async key => {
    await delay(50);
    if (cache.has(key)) {
      return Promise.resolve(true);
    }
    return Promise.resolve(false);
  };

  const resumePendingTasks = async key => {
    let result = await delayConcurrentTask(key);
    while (!result) {
      result = await delayConcurrentTask(key);
    }
    return Promise.resolve(true);
  };

  return async (req, res, next) => {
    const { searchQuery } = req.query;
    const cachedData = cache.get(searchQuery);
    if (cachedData != null) {
      const cacheTime = Date.now() - cachedData.timestamp;
      if (cacheTime > 60000) {
        setTimeout(() => updateCache(searchQuery), 0);
      }
      if (cachedData.searchResults && cachedData.searchResults.length > 0) {
        res.set("Cache-Control", "public, max-age=30");
      }
      res.json({ data: cachedData.searchResults });
    } else {
      const key = await fetchKey(searchQuery);
      if (key) {
        await resumePendingTasks(searchQuery);
        next();
      } else {
        await putKey(searchQuery);
        await updateCache(searchQuery);
        next();
      }
    }
  };
};
