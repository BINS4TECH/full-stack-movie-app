const axios = require("axios");
const config = require("config");
const winston = require("winston");
const OMDB_MOVIE_API = config.get("omdbAPIUrl");

axios.interceptors.response.use(null, error => {
  const expectedError =
    error.response &&
    error.response.status >= 400 &&
    error.response.status < 500;

  if (!expectedError) {
    winston.error("An unexpected error occurrred.");
  }

  return Promise.reject(error);
});

async function fetchRecords(URL, optPageNo, optLimit, optRecords) {
  const pageNo = optPageNo || 1;
  const limit = optLimit || 20;
  let allRecords = optRecords || [];
  const records = await axios
    .get(`${URL}&page=${pageNo}`)
    .then(res => {
      const searchResults = res.data && res.data.Search ? res.data.Search : [];
      // winston.info(searchResults);
      const totalResults =
        res.data && res.data.totalResults ? res.data.totalResults : 0;
      // winston.info(totalResults);
      allRecords = allRecords.concat(searchResults);
      if (
        allRecords &&
        allRecords.length > 0 &&
        allRecords.length < limit &&
        Number.parseInt(totalResults, 10) > limit
      ) {
        return fetchRecords(URL, pageNo + 1, limit, allRecords);
      }
      return allRecords;
    })
    .catch(error => {
      winston.error(error);
      throw error;
    });
  return records;
}

async function fetchMovies(data) {
  winston.info(`Initiating server call with = ${data.searchQuery}`);
  const URL = `${OMDB_MOVIE_API}&s=${encodeURIComponent(data.searchQuery)}`;
  const results = await fetchRecords(URL, 1, 20, []);
  return results;
}

module.exports = {
  fetchMovies
};
