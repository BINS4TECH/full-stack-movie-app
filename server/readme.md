## Introduction

Express node js application 

This is the backend of React front end app.

## Setup


### Install the Dependencies

Next, from the project folder, install the dependencies:

    npm i


### Run the Tests

You're almost done! Run the tests to make sure everything is working:

    npm test

All tests should pass.

### Start the Server

    node index.js

This will launch the Node server on port 3900. If that port is busy, you can set a different point in config/default.json.

Open up your browser and head over to:

http://localhost:3900/api/movies&searchQuery=action

You should see the list of movies. That confirms that you have set up everything successfully.


