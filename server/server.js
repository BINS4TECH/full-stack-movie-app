const winston = require("winston");
const express = require("express");
const config = require("config");
const path = require('path');
const app = express();

require("./startup/logging")();
require("./startup/cors")(app);
require("./startup/routes")(app);

const port = process.env.PORT || config.get("port");

if (process.env.NODE_ENV === 'production') {
  // Serve any static files
   app.use(express.static(path.join(__dirname, 'client/build')));

  // Handle React routing, return all requests to React app
  app.get('*', function(req, res) {
   res.sendFile(path.join(__dirname, 'client/build', 'index.html'));
  });
}

const server = app.listen(port, () =>
  winston.info(`Listening on port ${port}...`)
);

module.exports = server;
