import messages from '../messages/localeMessage.js';

export function fetchLocaleMessage(localeValue) {
  const localeMsg = messages[localeValue];
  return localeMsg ? localeMsg : {};
}
