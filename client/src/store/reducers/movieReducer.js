const INITIAL_STATE = {
  searchQuery: "",
  searchResults: null,
  userMessage: ""
};

export default (state = INITIAL_STATE, action) => {
  switch (action.type) {
    case "MOVIE_SEARCH_SUCCESS":
      return {
        ...state,
        searchQuery: action.searchQuery,
        searchResults: action.response ? action.response.data : [],
        userMessage: `Movie results with search term ${action.searchQuery}`
      };
    case "MOVIE_SEARCH_FAILURE":
      return {
        ...state,
        searchQuery: action.searchQuery,
        searchResults: null,
        userMessage: `API call error`
      };
    case "MOVIE_SEARCH_INPROGRESS":
      return {
        ...state,
        searchQuery: action.searchQuery,
        searchResults: null,
        userMessage : `Fetching results from server with search term ${action.searchQuery}`
      };
    default:
      return state;
  }
};
