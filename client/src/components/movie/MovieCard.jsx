import React from "react";
import PropTypes from "prop-types";
import Lazyload from "react-lazyload";
import LazyloadImage from "./LazyloadImage.jsx";

const MovieCard = ({ movie }) => (
  <React.Fragment>
    <div className='gallery-product col-lg-4 col-md-4 col-sm-4 col-xs-6'>
      <div className='card' style={{ height: '100%' }}>
        <div style={{ height: '90%' }}>
        <Lazyload height={'100px'}>
          <LazyloadImage
            imageUrl={movie.Poster}
            title={movie.Title}
            imgStyle={{height: '100%', width: '100%', 'objectFit': 'contain'}}
          />
        </Lazyload>
        </div>
        <div className='card-footer' style={{ height: '10%' }}>
          {movie.Title}
        </div>
      </div>
    </div>
  </React.Fragment>
);

MovieCard.defaultProps = {
  movie: {}
};

MovieCard.propTypes = {
  movie: PropTypes.object
};

export default MovieCard;
