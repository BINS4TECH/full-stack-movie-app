import React, { Component } from "react";
import PropTypes from "prop-types";
import Lazyload from "react-lazyload";

class LazyloadImage extends Component {
  componentDidMount() {
    const { title } = this.props;
    console.log(title);
  }
  render() {
    const { imageUrl, imgStyle } = this.props;
    return (
      <React.Fragment>
        <img src={imageUrl} alt="" style={imgStyle} />
      </React.Fragment>
    );
  }
}

LazyloadImage.propTypes = {
  height: PropTypes.string,
  imageUrl: PropTypes.string,
  title: PropTypes.string,
  imgStyle: PropTypes.object
};

export default LazyloadImage;
