# create-react-app React Project with Node Express Backend

> React app with a Node Express Backend

## Usage

Install [nodemon](https://github.com/remy/nodemon) globally

```
npm i nodemon -g
```

Install server and client dependencies

```
yarn
cd client
yarn
```

To start the server and client at the same time (from the root of the project)

```
yarn dev
```

Running the production build on localhost. This will create a production build, then Node will serve the app on http://localhost:3900

```
NODE_ENV=production yarn dev:server
```

## Client side implementaion

1. I used Redux as a state managent system and used a middleware to handle and monitor the user actions. If User invokes any API call to server, then this middleware handles all the cases(Inprogress, Success, Failure). This helped me to write a less code in the project as it is a generic function which dispatches inprogress, success, failure events. Using this aproach we get a full control of showing nice messages to user. We can add a notification bar or toast notification container in our component hierarchy.

## Server side implementaion.

I used express framework and one middleware function to cache user requests to handle external movie calls.


```
NODE_ENV=production yarn dev:server
```



